package at.ber.baumschule;

import at.ber.baumschule.FertilizationMethod;
import at.ber.baumschule.Tree;

public class Conifer extends Tree{

	public Conifer(int maxSize, int maxDiameter, FertilizationMethod FertilizationMethod) {
		super(maxSize, maxDiameter, FertilizationMethod);

	}

}
