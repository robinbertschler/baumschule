package at.ber.baumschule;

public interface FertilizationMethod {
	void fertilize();
}
