package at.ber.baumschule;

import java.util.List;
import java.util.ArrayList;

public class GrowingArea {
	public GrowingArea(String name, int size, List<Tree> trees) {
		super();
		this.name = name;
		this.size = size;
		this.trees = trees;
	}
	private String name;
	private int size;
	private List<Tree> trees;
}
