package at.ber.baumschule;

import java.util.ArrayList;
import java.util.List;

import at.ber.baumschule.Tree;
import at.ber.baumschule.LeafTree;
import at.ber.baumschule.Conifer;
import at.ber.baumschule.FertilizationMethod;
import at.ber.baumschule.SuperGrow;
import at.ber.baumschule.TopGreen;
import at.ber.baumschule.GrowingArea;

public class Main {
	public static void main(String[] args) {
		
		List<Tree> trees = new ArrayList();
		GrowingArea G1 = new GrowingArea("GROW1", 25, trees);
		FertilizationMethod tg = new TopGreen();
		FertilizationMethod sg = new SuperGrow();
		Tree conifer1 = new Conifer(12, 15,tg);
		Tree leaf1 = new LeafTree(10,20,sg);
		
		
	}
}
