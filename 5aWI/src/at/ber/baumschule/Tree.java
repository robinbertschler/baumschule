package at.ber.baumschule;

public abstract class Tree {
	public Tree(int maxSize, int maxDiameter, FertilizationMethod FertilizationMethod) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.FertilizationMethod = FertilizationMethod;
	}
	private int maxSize;
	private int maxDiameter;
	private FertilizationMethod FertilizationMethod;
	public FertilizationMethod getFertilizationMethod() {
		return FertilizationMethod;
	}
}
