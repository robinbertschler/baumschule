package at.ber.cds;


import java.util.ArrayList;

public class CD {
	
	private String name;
	private ArrayList<Song> songs = new ArrayList<Song>();
	
	public CD(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void addSong(Song s) {
		songs.add(s);
	}
	
	public ArrayList<Song> getSongs() {
		return songs;
	}

}