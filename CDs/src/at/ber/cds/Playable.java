package at.ber.cds;

public interface Playable {
	public void play();
	public void stop();
	public void pause();
}

