package at.ber.cds;

import java.util.ArrayList;


public class Song implements Playable{
	
	private String title;
	private int length;
	private ArrayList<CD> cds = new ArrayList<CD>();
	
	public Song(String title, int length) {
		this.title = title;
		this.length = length;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getLength(){
		return this.length;
	}
	
	public void addCD(CD cd) {
		cds.add(cd);
	}
	
	public ArrayList<CD> getAllCDsWithSong(){
		return cds;
	}

	@Override
	public void play() {
		System.out.println("Played song "+title);
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped song");
		
	}

	@Override
	public void pause() {
		System.out.println("Paused song");
		
	}

}