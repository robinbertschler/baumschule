package at.ber.cds;

import java.util.ArrayList;

public class Title implements Playable{

	private String title;
	private int length;
	private ArrayList<DVD> dvds = new ArrayList<DVD>();
	
	public Title(String title, int length) {
		this.title = title;
		this.length = length;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getLength(){
		return this.length;
	}
	
	public void addDVD(DVD cd) {
		dvds.add(cd);
	}
	
	public ArrayList<DVD> getAllDVDsWithSong(){
		return dvds;
	}

	@Override
	public void play() {
		System.out.println("Played "+title);
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped ");
		
	}

	@Override
	public void pause() {
		System.out.println("Paused ");
		
	}

}