package at.ber.cds;

import java.util.ArrayList;
import java.util.List;


public class main {

	public static void main(String[] args) {
		Actor a1 = new Actor("Elgho", "Amin");
		
		CD cd1 = new CD("CD1");
		a1.addCD(cd1);
		
		DVD dvd1 = new DVD("DVD1");
		a1.addDVD(dvd1);
		
		Song s1 = new Song("CD Song Nr.1", 12);
		Title t1 = new Title("DVD Title Nr.1",12);
		
		cd1.addSong(s1);
		dvd1.addTitle(t1);
		
		Player p = new Player();
		p.addPlayable(s1);
		p.addPlayable(t1);
		
		p.playAll();
	}

}
