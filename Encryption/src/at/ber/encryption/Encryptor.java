package at.ber.encryption;

import java.util.Scanner;
 
public interface Encryptor {
	public String encrypt(String string);
	public String decrypt(String string);
}