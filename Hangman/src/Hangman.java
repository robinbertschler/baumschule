import java.util.Scanner;

public class Hangman {

	private static String[] w�rter = { "stuhl", "haus", "blum", "pflanz", "k�ch", "traube" };
	private static String wort = w�rter[(int) (Math.random() * w�rter.length)];
	private static String stern = new String();
	
	static int count = 0;

	public static void main(String[] args) {
		

		stern=(wort.replace("e", "*").replace("a", "*").replace("o", "*").replace("u", "*").replace("p", "*").replace("t", "*"));
	
		Scanner sc = new Scanner(System.in);

		while (count < 8 && stern.contains("*")) {
			System.out.println("Buchstaben erraten");
			System.out.println(stern);
			String raten = sc.next();
			ermitteln(raten);
		}
		sc.close();
	}

	public static void ermitteln(String guess) {
		String sternneu = "";
		for (int i = 0; i < wort.length(); i++) {
			if (wort.charAt(i) == guess.charAt(0)) {
				sternneu += guess.charAt(0);
			} else if (stern.charAt(i) != '*') {
				sternneu += wort.charAt(i);
			} else {
				sternneu += "*";
			}
		}

		if (stern.equals(sternneu)) {
			count++;
			grafik();
		} else {
			stern = sternneu;
		}
		if (stern.equals(wort)) {
			System.out.println("Gratuliere: " + wort);
		}
	}

	public static void grafik() {
		if (count == 1) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("-->          |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 2) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("---->        |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 3) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("------>       |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 4) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("-------->      |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 5) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("---------->     |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 6) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("------------>    |");
			System.out.println();
			System.out.println();
			System.out.println();
		}
		if (count == 7) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("-------------->   |");
			System.out.println();
			System.out.println();
			System.out.println();

		}
		if (count == 8) {
			System.out.println("Falsch");
			System.out.println();
			System.out.println("------------------X");
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("Verloren: " + wort);
		}
	}
}