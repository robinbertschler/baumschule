package at.ber.observerpatterns;

public class Lantern implements Observable {

	@Override
	public void inform() {
		System.out.println("i was informed");

	}

}
