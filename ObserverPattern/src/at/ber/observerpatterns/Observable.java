package at.ber.observerpatterns;

public interface Observable {
	public void inform();

}
