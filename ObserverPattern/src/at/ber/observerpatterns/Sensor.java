package at.ber.observerpatterns;


import java.util.ArrayList;
import java.util.List;

public class Sensor {

	private List<Observable> Observables;

	public Sensor() {
		super();
		this.Observables = new ArrayList<Observable>();

	}

	public void addObservable(Observable O1) {
		this.Observables.add(O1);
	}

	public void informAll() {
		for(Observable o : this.Observables) {
			o.inform();
		}
	}
}
