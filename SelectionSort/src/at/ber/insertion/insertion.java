package at.ber.insertion;

public class insertion {

	void insertionsort(int arr[]) {
		int n = arr.length;
		for (int i = 1; i < n; i++) {
			int temp = arr[i];

			while (i > 0 && arr[i - 1] > temp) {
				arr[i] = arr[i - 1];
				i--;
			}
			arr[i] = temp;

		}
	}

	void printArray(int arr[]) {
		int n = arr.length;
		for (int i = 0; i < n; ++i)
			System.out.print(arr[i] + " ");
		System.out.println();
	}

	public static void main(String args[]) {
		insertion ob = new insertion();
		int arr[] = { 64, 34, 25, 12, 22, 11, 99 };
		ob.insertionsort(arr);
		System.out.println("Sorted array");
		ob.printArray(arr);
	}
}