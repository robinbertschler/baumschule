import java.util.Arrays;
import java.util.Scanner;

public class TicTacToe {
	static Scanner spielereingabe;
	static String[] feld;
	static String spielerwechsel;

	public static void main(String[] args) {
		spielereingabe = new Scanner(System.in);
		feld = new String[9];
		spielerwechsel = "X";
		String gewinner = null;
		feldf�llen();
		feldAnzeigen();
		System.out.println("X beginnt");
		int eingabe;

		while (gewinner == null) {

			eingabe = spielereingabe.nextInt();
				
			if (!(eingabe > 0 && eingabe <= 9)) {
				System.out.println("Falsche eingabe");
				continue;
			}

			if (feld[eingabe - 1].equals(String.valueOf(eingabe))) {
				feld[eingabe - 1] = spielerwechsel;
				if (spielerwechsel.equals("X")) {
					spielerwechsel = "O";
				} else {
					spielerwechsel = "X";
				}
				feldAnzeigen();
				gewinner = checkWinner();
			} else {
				System.out.println("Slot bereits gef�llt");
				continue;
			}
		}


	}

	static String checkWinner() {
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = feld[0] + feld[1] + feld[2];
				break;
			case 1:
				line = feld[3] + feld[4] + feld[5];
				break;
			case 2:
				line = feld[6] + feld[7] + feld[8];
				break;
			case 3:
				line = feld[0] + feld[3] + feld[6];
				break;
			case 4:
				line = feld[1] + feld[4] + feld[7];
				break;
			case 5:
				line = feld[2] + feld[5] + feld[8];
				break;
			case 6:
				line = feld[0] + feld[4] + feld[8];
				break;
			case 7:
				line = feld[2] + feld[4] + feld[6];
				break;
			}
			if (line.equals("XXX")) {
				System.out.println("Der Gewinner ist: X");
				System.exit(0);
			} else if (line.equals("OOO")) {
				System.out.println("Der Gewinner ist: O");
				System.exit(0);
			}
		}

		for (int a = 0; a < 9; a++) {
			if (Arrays.asList(feld).contains(String.valueOf(a+1))) {
				break;
			}else if (a==8) { System.out.println("Unentschieden"); System.exit(0);}
		}

		System.out.println(spielerwechsel + " ist dran");
		return null;
	}

	static void feldf�llen() {
		for (int a = 0; a < 9; a++) {
			feld[a] = String.valueOf(a + 1);
		}
	}

	static void feldAnzeigen() {
		System.out.println("-------------");
		System.out.println("| " + feld[0] + " | " + feld[1] + " | " + feld[2] + " |");
		System.out.println("-------------");
		System.out.println("| " + feld[3] + " | " + feld[4] + " | " + feld[5] + " |");
		System.out.println("-------------");
		System.out.println("| " + feld[6] + " | " + feld[7] + " | " + feld[8] + " |");
		System.out.println("-------------");
	}

}
