package at.ber.cds;

import java.util.List;



public class Performer {
	
	private String firstname;
	private String lastname;
	
	public Performer(String firstname, String lastname, List<CD> cds, List<DVD> dvds) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.cds = cds;
		this.dvds = dvds;
	}

	
	private List<CD> cds;
	private List<DVD> dvds;
}
