package at.ber.cds;

public interface Playable {
		void play();
		void stop();
		void pause();
}
