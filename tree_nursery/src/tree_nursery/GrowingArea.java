package tree_nursery;

import java.util.List;
import java.util.ArrayList;

public class GrowingArea {
	public GrowingArea(String name, int size, List<tree> trees) {
		super();
		this.name = name;
		this.size = size;
		this.trees = trees;
	}
	private String name;
	private int size;
	private List<tree> trees;
}
